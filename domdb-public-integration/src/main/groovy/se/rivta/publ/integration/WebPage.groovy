package se.rivta.publ.integration

import groovy.transform.Immutable


@Immutable
class WebPage {
    String fileName
    String contents

    byte[] getBytes(){
        return contents.getBytes("UTF-8")
    }
}

package se.rivta.publ.integration

import groovy.util.logging.Log
import org.apache.http.HttpEntity
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils

import java.nio.charset.Charset

@Log
class PublicPages {
    private CloseableHttpClient httpClient = HttpClients.createMinimal()
    private String baseUrl

    def PublicPages(String url) {
        this.baseUrl = url.endsWith("/") ? url : url + "/"
    }

    WebPage createDomainListPage() {
        String pageName = "index.html"

        WebPage page = getWebPage(pageName)
        return page
    }

    WebPage createInteractionListPage() {
        String pageName = "interaction_index.html"

        WebPage page = getWebPage(pageName)
        return page
    }

    WebPage createDomainTypePage(Integer domainType) {
        String pageName = "type-"+ domainType + ".html"
        WebPage page = getWebPage(pageName)
        return page
    }

    WebPage createDomainDetailsPage(String domainName) {
        String pageName = domainName.replace(':','_').replace('.','_') + ".html"

        WebPage page = getWebPage(pageName)
        return page
    }

    private WebPage getWebPage(String pageName) {
        String url = baseUrl + pageName

        log.info("Getting ${url}")

        def request = new HttpGet(url)
        CloseableHttpResponse httpResponse = httpClient.execute(request);

        try {
            if (httpResponse.getStatusLine().statusCode != 200) {
                throw new Exception("HTTP Error: ${httpResponse.getStatusLine().reasonPhrase}")
            }

            HttpEntity entity = httpResponse.getEntity()
            String content = EntityUtils.toString(entity, Charset.defaultCharset())

            def page = new WebPage(fileName: pageName, contents: content)
            return page
        } finally {
            httpResponse.close();
        }
    }
}

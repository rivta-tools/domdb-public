package se.rivta.publ.integration

import groovy.util.logging.Log
import org.apache.commons.vfs2.FileContent
import org.apache.commons.vfs2.FileObject

@Log
class PagePublisher {
    FileObject connector

    def PagePublisher(FileObject folderConnector){
        connector = folderConnector
    }

    def publish(WebPage page) {
        log.info("Publishing ${page.fileName}")

        FileObject fileObject = connector.resolveFile(page.fileName)
        FileContent fileContent = fileObject.getContent()
        def stream = fileContent.getOutputStream()

        def bytes = page.contents.getBytes("UTF-8")
        try {
            stream.write(bytes)
        } finally {
            stream.close()
        }
    }
}

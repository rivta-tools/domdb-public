#!/usr/bin/env groovy

package groovy

import org.apache.commons.vfs2.FileSystemManager
import org.apache.commons.vfs2.FileSystemOptions
import org.apache.commons.vfs2.VFS
import org.apache.commons.vfs2.provider.ftps.FtpsFileSystemConfigBuilder
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder
@Grapes([
        @Grab(group = 'se.rivta.domdb.domain', module = 'domdb-domain-core', version = '1.9.0'),
        @Grab(group = 'se.rivta.domdb.public', module = 'domdb-public-integration', version = '1.3.5'),
        @Grab(group = 'org.hibernate', module = 'hibernate-entitymanager', version = '4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab(group = 'mysql', module = 'mysql-connector-java', version = '5.1.34'),
        @GrabConfig(systemClassLoader = true),
        @Grab(group = 'org.apache.httpcomponents', module = 'httpclient', version = '4.3.6'),
        @Grab(group = 'commons-httpclient', module = 'commons-httpclient', version = '3.1'),
        @Grab(group = 'commons-net', module = 'commons-net', version = '2.2'),
        @Grab(group = 'org.apache.commons', module = 'commons-vfs2', version = '2.0'),
        @Grab(group = 'ch.qos.logback', module = 'logback-classic', version = '1.2.3'),
        @Grab(group = 'net.logstash.logback', module = 'logstash-logback-encoder', version='6.4'),
        @Grab(group='org.slf4j', module='jul-to-slf4j', version='1.7.22'),
        @Grab(group='com.jcraft', module='jsch', version='0.1.53')
])

import se.rivta.domdb.domain.core.Domain

import se.rivta.publ.integration.PagePublisher
import se.rivta.publ.integration.PublicPages

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import javax.persistence.Query

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import groovy.transform.Field

SLF4JBridgeHandler.removeHandlersForRootLogger();
SLF4JBridgeHandler.install();

@Field
def Logger logger = LoggerFactory.getLogger("scriptLogger")

if (args.size() < 2){
    println("Arguments: <sourceUrl> <destinationConnectionString>")
    println("- <sourceUrl> is the base path, i.e. http://localhost/public")
    println("- <destinationConnectionString> is in the format protocol://username:password@host/folder")
    return
}

def sourceUrl = args[0]
def publishUrl = args[1]

def configFileName = "${System.getenv("domdb_config_dir")}/domains-jpa.properties"
def configStream = new FileInputStream(configFileName)
def jpaProperties = new Properties()
jpaProperties.load(configStream)

EntityManagerFactory factory = Persistence.createEntityManagerFactory("datasource-domains", jpaProperties)
EntityManager manager = factory.createEntityManager()

def publicPages = new PublicPages(sourceUrl)

def webPages = []
webPages << publicPages.createDomainListPage()
webPages << publicPages.createInteractionListPage()

Query domainQuery = manager.createQuery("select d from Domain d");
List<Domain> domains = domainQuery.getResultList()
for(domain in domains){
    webPages << publicPages.createDomainDetailsPage(domain.name)
}

Query queryDomainTypes = manager.createQuery("select d from DomainType d");
List<Domain> domainTypes = queryDomainTypes.getResultList()
for(domainType in domainTypes){
    webPages << publicPages.createDomainTypePage(domainType.id)
}

manager.close()
factory.close()

FileSystemManager ftpManager = VFS.getManager();
def fileSystemOptions = new FileSystemOptions()
SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(fileSystemOptions, "no");
FtpsFileSystemConfigBuilder.getInstance().setPassiveMode(fileSystemOptions, true);
FtpsFileSystemConfigBuilder.getInstance().setUserDirIsRoot(fileSystemOptions, false);

def ftpFolder = ftpManager.resolveFile(publishUrl, fileSystemOptions);

def publisher = new PagePublisher(ftpFolder)

webPages.each { page ->
    publisher.publish(page)
}

logger.info("Success!")
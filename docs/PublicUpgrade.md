## Zip-fil
Till att börja med behövs en zip-fil för den release du vill installera. 
Ladda ner zip-filen och kopiera den till din domdb-mapp på servern. Packa sedan upp den med följande kommando:

```
unzip domdb-public-x.y.z.zip
```
zip-filen skapas i katalogen domdb-domain-package/target/domdb-domain-x.x.x.bin.zip Innehåller följande artifakt:
  *  /bin - war filer, .jar filer som behövs för att köra .groovy skript filer
  *  /scripts - groovy skript filer

## Uppgradera webbapplikation
Gör följande för att uppgradera public-webbapplikationen

  * Avinstallera den befintliga public.war från din servlet container
  * Installera den nya war-filen från mappen domdb-public-rivtaweb

För Tomcat-instruktioner, se [TomcatCheatSheet](TomcatCheatSheet.md).
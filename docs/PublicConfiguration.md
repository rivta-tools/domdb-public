## Förberedelser
Dessa komponenter kräver att följande först är installerat och konfigurerat.

  * Common, se [CommonConfiguration](CommonConfiguration.md)
  * Domain, se [DomainConfiguration](DomainConfiguration.md)
  * Connection, se [ConnectionConfiguration](ConnectionConfiguration.md)
  * En **servlet container**, för Tomcat på Ubuntu, se [TomcatCheatSheet](TomcatCheatSheet.md)

## Databaser
Public-komponenterna har inga egna databaser, utan använder data från övriga komponenter.

## Installation av programvara
Kopiera och packa upp den senaste domdb-public-x.y.z-bin.zip i den programmapp som skapades i steget CommonConfiguration, t.ex. ~/domdb.

```
unzip domdb-public-x.y.z-bin.zip
```

## Konfigurera webbdelar

### Miljövariabler
Följande miljövariabler behöver finnas tillgängliga för din servlet container:

**domdb\_config\_dir** - ska peka på den mapp som innehåller konfigurationsfiler

**CATALINA\_OPTS** - kan behöva följande inställningar för minneshanteringen:

```
export CATALINA_OPTS="$CATALINA_OPTS -server -Xms128m -Xmx1024m -XX:PermSize=64m -XX:MaxPermSize=256m"
```

I Tomcat kan miljövariabler sättas via filen **/usr/share/tomcat7/bin/setenv.sh**. Om detta gjordes i steget [AdminWebConfiguration](AdminWebConfiguration.md) behöver det inte göras igen.

### Installera war-filer
Bland de uppackade filerna från domdb-public-x.y.z.zip finns en fil med namn domdb-public-rivtaweb-x.y.z.war. Installera denna i din servlet container. Om du har möjlighet, välj context path "public", vilket ger webbadressen http://servernamn:8080/public.

För Tomcat-instruktioner, se [TomcatCheatSheet](TomcatCheatSheet.md).

### Kontrollera
Nu ska du kunna surfa till applikationen, via den adress som är konfigurerad i din servlet container, t.ex. http://servernamn:8080/public eller http://servernamn:8080/domdb-public-rivtaweb-x.y.z
OBS! Repositoriet är deprecated!


Detta repository innehåller källkoden till domänsidorna på http://rivta.se, som ett Grails-projekt. Dessutom finns script för att kopiera sidorna från en intern server till ett webbhotell.

# Utvecklarinstruktioner

Detta är ett s.k. Maven "multi module project", med ett "parent project" (domdb-public) och följande "sub-modules":

* domdb-public-integration - kod och script för att uppdatera rivta.se utifrån en lokal tomcat-instans.
* domdb-public-rivtaweb - webbapplikation fsom genererar publika domänsidor till rivta.se, byggd med webbramverket Grails.
* domdb-public-package - innehåller Maven-konfiguration för att skapa en release-zip med jar- och war-filer, groovy-script m.m.

## Programvaror
Du behöver följande programvaror installerade för att kunna köra koden i "utvecklarläge", samt för att paketera releaser:

* Java Development Kit (version 1.7+) - behöver vara installerad och miljövariabeln JAVA_HOME satt till JDK-installationens rotkatalog (ett steg ovanför javas bin-katalog)
* Groovy (version 2.4+) - behöver vara uppackad och dess bin-katalog finnas med i PATH
* Grails (version 2.5) - behöver vara uppackad och dess bin-katalog finnas med i PATH
* Maven (version 3+) - behöver vara uppackas och dess bin-katalog finnas med i PATH
* MySQL (version 5.5+) - behöver vara installerad och du ha ett konto med hög behörighet

## Konfigurera utvecklingsmiljön
### Klona Git-repository
Klona detta git-repository via en Git-klient, alternativt ladda ned källkoden som en zip.

### Databaser
Denna webbapplikation återanvänder de befintliga databaserna från domdb-connection och domdb-domain. Inga ytterligae databaser behöver skapas. 

### Miljövariabel
Webbapplikation och script återanvänder miljövariabeln **domdb_config_dir**, som tidigare skapats.

### Skapa inställningsfiler
Skapa följande inställningsfil i katalogen:

#### rivtaweb-config.groovy
Denna konfigurationsfil används av Grails web-projekt, och pekar ut sökvägen till både domän- och anslutningsdatabasen.

```
dataSource_domains {
    url = 'jdbc:mysql://<servernamn>:3306/<domändatabasnamn>'
    username='<användare>'
    password='<lösenord>'
}

dataSource_connections {
    url = 'jdbc:mysql://<servernamn>:3306/<connectiondatabasnamn>'
    username = '<användare>'
    password = '<lösenord>'
}
```


## Köra programvaran
Innan någon av komponenterna kan exekveras så behöver jar-filerna installeras i din lokala Maven-cache. Vissa utvecklingsverktyg kan göra att detta steg inte behövs.

1. Starta en kommandoprompt och gå till domdb-public (rotkatalogen i det klonade git-repositoriet)
2. Installera jar-filerna så att de hittas av webapplikationen: mvn install


### Rivtaweb-projektet
Rivtaweb-projektet körs i en utvecklinsgmiljö så här:

1. Starta en kommandoprompt och gå till katalogen domdb-public-rivtaweb i källkodsträdet
2. Kör kommandot mvn grails:run-app


### Publicera webbsidor
För att testa en publicering brukar vi ladda upp sidor till katalogen "domainsx" istället för den vanliga "domains" på rivta.se. 

Gör så här:

1. Lämna rivtaweb-projektet igång! Det behövs för att kunna läsa sidorna.
2. Öppna en ytterligare kommandoprompt och gå till mappen domdb-public-integration/src/main/scripts
3. Kör följande kommando:

```
groovy generatePublicDomainPages.groovy http://localhost:8080/domdb-public-rivtaweb/domains ftp://username:password@ftp.rivta.se/domainsx
```



## Skapa en release

### Uppdatera versionsnummer
Versionsnumret behöver uppdateras i alla pom-filer. Det görs enklast med följande kommando:

```
cd <projektets rotkatalog>
mvn versions:set -DgenerateBackupPoms=False -DnewVersion=x.y.z
```

pom.xml i rotkatalogen ska nu ha fått det nya versionsnumret (under "version") och under-modulerna ska ha fått samma versionsnummer (under "parent/version").

Om det är viktigt att något script använder exakt denna nya version, uppdatera versionsnumret manuellt i Grapes/Grab-avsnittet längst upp i respektive script.

### Bygga release
Nu kan du bygga en release-zip med följande kommando:

```
mvn package
```

zip-filen skapas i katalogen domdb-public-package/target

### Checka in ändringar
Om allt ser bra ut:

* committa dina ändringar till Git, 
* skapa en tag med samma namn som versionsnumret
* pusha alltsammans till Bitbucket (dubbelkolla att taggen syns på Bitbucket).

### Configuration/Uppdatering

  * [PublicConfiguration](docs/PublicConfiguration.md).
  * [PublicUpgrade](docs/PublicUpgrade.md).
  
package se.rivta.domdb.publ.rivtaweb

import java.util.regex.Matcher


class StringFormatting {
    public static String cutStringIfLongerThan(String input, int length) {
        def pos = input?.indexOf('.')
        if (pos > 0 && input.length() > length) {
            return input.substring(0, pos + 1)
        } else {
            return input
        }
    }

    static String injectSoftLineBreaksBeforeCapitals(String input) {
        Matcher matcher = input =~ /[A-Z]([a-z]*)/

        def result = new StringBuilder()

        while (matcher.find()) {
            result.append(input.substring(matcher.start(), matcher.end()))
            result.append("&shy;")
        }

        return result.toString()
    }
}

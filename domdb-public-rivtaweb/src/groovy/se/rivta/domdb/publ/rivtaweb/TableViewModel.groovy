package se.rivta.domdb.publ.rivtaweb


class TableViewModel {
    def TableViewModel() {
        headings = new HashMap<String, String>()
        rows = new HashMap<String, TableRowViewModel>()
    }

    Map<String, String> headings
    Map<String, TableRowViewModel> rows
}

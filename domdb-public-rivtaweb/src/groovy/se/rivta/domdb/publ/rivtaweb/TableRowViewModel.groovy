package se.rivta.domdb.publ.rivtaweb


class TableRowViewModel {
    def TableRowViewModel() {
        cells = new HashMap<String, Object>()
    }

    String title
    String description
    Map<String, Object> cells
}

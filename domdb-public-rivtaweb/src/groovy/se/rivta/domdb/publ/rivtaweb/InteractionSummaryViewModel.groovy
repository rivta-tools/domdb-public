package se.rivta.domdb.publ.rivtaweb

class InteractionSummaryViewModel {
    String name
    String description
    String domainName
    String domainPage
    Date lastUpdated
    boolean takProd
    boolean takTest
}

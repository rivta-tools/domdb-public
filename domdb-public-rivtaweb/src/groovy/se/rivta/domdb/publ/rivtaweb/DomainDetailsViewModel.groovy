package se.rivta.domdb.publ.rivtaweb

import grails.util.Pair

class DomainDetailsViewModel {
    def DomainDetailsViewModel() {
        specifications = new TableViewModel()
        reviews = new TableViewModel()
        serviceContracts = new TableViewModel()
        producers = new TableViewModel()
        consumers = new TableViewModel()
    }

    String englishName
    String swedishShortName
    String swedishLongName
    String description
    Pair<Integer, String> domainType

    String infoPageUrl
    String issueTrackerUrl
    String sourceCodeUrl

    TableViewModel specifications
    TableViewModel reviews
    TableViewModel serviceContracts
    TableViewModel producers
    TableViewModel consumers
}

package se.rivta.domdb.publ.rivtaweb

class DomainSummaryViewModel {

    String englishName
    String swedishShortName
    String swedishLongName
    String detailsPage
    boolean takProd
    boolean takTest
}

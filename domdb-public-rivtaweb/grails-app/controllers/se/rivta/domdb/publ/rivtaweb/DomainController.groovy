package se.rivta.domdb.publ.rivtaweb

import grails.util.Pair
import org.hibernate.criterion.Restrictions
import se.rivta.domdb.connection.core.ServiceContract
import se.rivta.domdb.connection.core.Tak
import se.rivta.domdb.domain.core.DescriptionDocument
import se.rivta.domdb.domain.core.DescriptionDocumentType
import se.rivta.domdb.domain.core.Domain
import se.rivta.domdb.domain.core.DomainType
import se.rivta.domdb.domain.core.DomainVersion
import se.rivta.domdb.domain.core.InteractionDescription
import se.rivta.domdb.domain.core.Review

class DomainController {

    def sessionFactory_domains
    def sessionFactory_connections

    def index() {
        def domainDbSession = sessionFactory_domains.getCurrentSession()
        List<Domain> domains = domainDbSession.createCriteria(Domain.class).add(Restrictions.eq("hidden", false)).list()

        def connectionsDbSession = sessionFactory_connections.getCurrentSession()
        List<Tak> takObjects = connectionsDbSession.createCriteria(Tak.class).list()

        def connectionDbSession = sessionFactory_connections.getCurrentSession()
        List<ServiceContract> takContracts = connectionDbSession.createCriteria(ServiceContract.class).list()

        def modelsStructure = createModelStructure(domainDbSession)
        def groupedViewModels = sort(buildDomainSummaryViewModels(modelsStructure, domains, takContracts))

        Tak takProd = takObjects.find { it.id == 1 }
        Tak takTest = takObjects.find { it.id == 2 }

        return [takProdName: takProd?.name,
                takTestName: takTest?.name,
                viewModels:groupedViewModels
        ]
    }

    private HashMap<DomainType, List<DomainSummaryViewModel>> createModelStructure(domainDbSession) {
        List<DomainType> types = domainDbSession.createCriteria(DomainType.class).list()

        def groupedModels = new HashMap<DomainType, List<DomainSummaryViewModel>>()
        types.each { type -> groupedModels.put(type, new LinkedList<DomainSummaryViewModel>()) }

        return groupedModels
    }

    private Map buildDomainSummaryViewModels(
            HashMap<DomainType, List<DomainSummaryViewModel>> modelStructure, domains, takContracts) {

        for (Domain domain in domains) {
            def viewModel = new DomainSummaryViewModel()
            viewModel.englishName = domain.name
            viewModel.swedishShortName = domain.swedishShort
            viewModel.swedishLongName = domain.swedishLong?.replace(":", ": ")
            viewModel.detailsPage = domain.name.replace(':', '_').replace('.', '_') + ".html"
            viewModel.takProd = takContracts.any { it.namespace.contains(domain.name) && it.tak.id == 1 }
            viewModel.takTest = takContracts.any { it.namespace.contains(domain.name) && it.tak.id == 2 }
            modelStructure.get(domain.domainType)?.add(viewModel)
        }
        return modelStructure
    }

    private Map sort(HashMap<DomainType, List<DomainSummaryViewModel>> modelStructure) {
        modelStructure.each { group ->
            group.value.sort(
                    { model -> model.englishName })
        }
        modelStructure.sort({ it.key.id })
    }

    def show(String domainName) {
        def hib = sessionFactory_domains.getCurrentSession()

        // In the domainName parameter the domain name parts are separated with '_'
        // In the database they could be separated by ':' or by '.'
        String domainMatch = domainName.replace('_', '%')
        def criteria = hib.createCriteria(Domain.class)
        criteria.add(Restrictions.like("name", domainMatch))

        Domain domain = criteria.uniqueResult()

        if (domain == null) {
            render(status: 404, text: 'Not found.')
            return
        }

        def viewModel = new DomainDetailsViewModel()
        viewModel.englishName = domain.name
        viewModel.swedishShortName = domain.swedishShort
        viewModel.swedishLongName = domain.swedishLong
        viewModel.description = domain.description?.replace("\n", "<br/>")

        viewModel.issueTrackerUrl = domain.issueTrackerUrl
        viewModel.infoPageUrl = domain.infoPageUrl
        viewModel.sourceCodeUrl = domain.sourceCodeUrl
        viewModel.domainType = new Pair(domain.domainType.id, domain.domainType.name)
        List<DomainVersion> versions = domain.versions.findAll { !it.hidden }.sort { it.name }

        viewModel.specifications = buildSpecificationsViewModel(versions)
        viewModel.reviews = buildReviewsViewModel(versions)
        viewModel.serviceContracts = buildContractsViewModel(domain)

        if (domain.serviceContracts) {
            def allContractNamespaces = domain.serviceContracts.collect { it.namespace }
            def connectionDbSession = sessionFactory_connections.getCurrentSession()
            def connectionContractsCriteria = connectionDbSession.createCriteria(ServiceContract.class)
            connectionContractsCriteria.add(Restrictions.in("namespace", allContractNamespaces.toArray()))
            connectionContractsCriteria.add(Restrictions.eq("tak.id", 1))
            List<ServiceContract> connectionContracts = connectionContractsCriteria.list()

            viewModel.producers = buildConnectionsViewModel(connectionContracts, "routes")
            viewModel.consumers = buildConnectionsViewModel(connectionContracts, "authorizations")
        }

        return [viewModel: viewModel]
    }

    def showDomainTypeInfo(Integer domainType){
        def hib = sessionFactory_domains.getCurrentSession()
        def criteria = hib.createCriteria(DomainType.class)
        criteria.add(Restrictions.eq("id", domainType))
        DomainType type = criteria.uniqueResult()

        if (type == null) {
            render(status: 404, text: 'Not found.')
            return
        }

        return [domainType: type]
    }

    private TableViewModel buildSpecificationsViewModel(List<DomainVersion> versions) {
        def viewModel = new TableViewModel()

        viewModel.rows["TKB"] = new TableRowViewModel(title: "TKB")
        viewModel.rows["AB"] = new TableRowViewModel(title: "AB")
        viewModel.rows["IS"] = new TableRowViewModel(title: "IS")
        viewModel.rows["ZIP"] = new TableRowViewModel(title: "ZIP")

        for (DomainVersion version in versions) {

            viewModel.headings[version.name] = version.name

            def tkbDocument = version.descriptionDocuments.find { it.documentType == DescriptionDocumentType.TKB }

            if (tkbDocument) {
                viewModel.rows["TKB"].cells[version.name] = buildFileViewModel(version, tkbDocument)
            }

            def abDocument = version.descriptionDocuments.find { it.documentType == DescriptionDocumentType.AB }

            if (abDocument) {
                viewModel.rows["AB"].cells[version.name] = buildFileViewModel(version, abDocument)
            }

            def isDocument = version.descriptionDocuments.find { it.documentType == DescriptionDocumentType.IS }

            if (isDocument) {
                viewModel.rows["IS"].cells[version.name] = buildFileViewModel(version, isDocument)
            }

            if (version.zipUrl) {
                def zipFileViewModel = new FileViewModel()
                zipFileViewModel.name = "Zip"
                zipFileViewModel.url = version.getZipUrl()
                viewModel.rows["ZIP"].cells[version.name] = zipFileViewModel
            }

        }

        return viewModel
    }

    private FileViewModel buildFileViewModel(DomainVersion version, DescriptionDocument document) {
        def tkbFileViewModel = new FileViewModel()
        tkbFileViewModel.name = document.documentType.name()

        def fileUrl = new URL(version.getFullDocumentsPath() + "/" + document.fileName)

        if (fileUrl.host.contains("bitbucket")) {
            tkbFileViewModel.url = fileUrl.toString().replace("/src/", "/raw/")
        } else {
            tkbFileViewModel.url =  fileUrl.toString()
        }

        return tkbFileViewModel
    }

    private TableViewModel buildReviewsViewModel(List<DomainVersion> versions) {
        def viewModel = new TableViewModel()

        for (DomainVersion version in versions) {
            viewModel.headings[version.name] = version.name

            version.reviews.each { Review review ->
                if (!viewModel.rows[review.reviewProtocol.code]){
                    viewModel.rows[review.reviewProtocol.code] = new TableRowViewModel(title: review.reviewProtocol.name)
                }

                def reviewFileViewModel = new FileViewModel()
                reviewFileViewModel.name = review.reviewOutcome.name
                reviewFileViewModel.url = review.reportUrl

                viewModel.rows[review.reviewProtocol.code].cells[version.name] = reviewFileViewModel
            }
        }

        return viewModel
    }

    private TableViewModel buildContractsViewModel(Domain domain) {
        def viewModel = new TableViewModel()

        domain.versions.findAll {!it.hidden} .collect { it.name }.sort().each {
            viewModel.headings[it] = it
        }

        def interactionNames = new HashSet<String>()
        domain.interactions.findAll { it ->
            if (it.interactionDescriptions == null || it.interactionDescriptions?.size() == 0) return false
            // Interactions could be removed from all domain versions, but left in our database, "orphans"...
            for (def description : it.interactionDescriptions) {
                if (!description.domainVersion?.hidden) return true;
            }
            return false
        }.each { interactionNames.add(it.name) }

        interactionNames.sort().each { String name ->
            def rowViewModel = new TableRowViewModel(title: name.replace("Interaction", ""))
            InteractionDescription latestDescription = null

            domain.versions.each { DomainVersion domainVersion ->
                def descriptions = domainVersion.interactionDescriptions.findAll {
                    it.interaction.name == name
                }

                descriptions.each { InteractionDescription description ->
                    rowViewModel.cells[domainVersion.name] = "${description.interaction.major}.${description.interaction.minor}"

                    if (!latestDescription || latestDescription.lastChangedDate < description.lastChangedDate) {
                        latestDescription = description
                    }
                }
            }

            if (latestDescription) {
                rowViewModel.description = StringFormatting.cutStringIfLongerThan(latestDescription.description, 120)
            }

            viewModel.rows[name] = rowViewModel
        }

        return viewModel
    }

    private TableViewModel buildConnectionsViewModel(List<ServiceContract> connectionContracts, String connectionAttribute) {
        def viewModel = new TableViewModel()

        connectionContracts.collect { extractShortName(it.namespace) } .sort().each {
            viewModel.headings[it] = StringFormatting.injectSoftLineBreaksBeforeCapitals(it.replace("Responder", ""))
        }

        connectionContracts.each { ServiceContract serviceContract ->
            serviceContract[connectionAttribute].each {
                // it == Route or CallAuthorization object
                def serviceComponent = it.serviceComponent
                if (!viewModel.rows[serviceComponent.hsaId]) {
                    viewModel.rows[serviceComponent.hsaId] = new TableRowViewModel(title: serviceComponent.description, description: serviceComponent.hsaId)
                }

                def serviceComponentRow = viewModel.rows[serviceComponent.hsaId]
                def shortName = extractShortName(serviceContract.namespace)

                if (!serviceComponentRow.cells[shortName]) {
                    serviceComponentRow.cells[shortName] = new TreeSet()
                }

                serviceComponentRow.cells[shortName].add("${extractMajorVersion(serviceContract.namespace)}.x")
            }

        }

        return viewModel
    }

    private String extractShortName(String namespace) {
        String[] parts = namespace.split(':')
        return parts[parts.length - 2]
    }

    private String extractMajorVersion(String namespace) {
        String[] parts = namespace.split(':')
        return parts[parts.length - 1]
    }
}
package se.rivta.domdb.publ.rivtaweb

import org.hibernate.criterion.Restrictions
import se.rivta.domdb.connection.core.ServiceContract
import se.rivta.domdb.connection.core.Tak
import se.rivta.domdb.domain.core.DomainType
import se.rivta.domdb.domain.core.Interaction
import se.rivta.domdb.domain.core.InteractionDescription

class InteractionController {

    def sessionFactory_domains
    def sessionFactory_connections

    def index() {
        def domainDbSession = sessionFactory_domains.getCurrentSession()

        List<Interaction> results = domainDbSession.createCriteria(Interaction.class).createAlias("domain", "domain").
                add(Restrictions.eq("domain.hidden", false)).list()

        def connectionsDbSession = sessionFactory_connections.getCurrentSession()
                List<Tak> takObjects = connectionsDbSession.createCriteria(Tak.class).list()

        def interactionMap = results.groupBy ( { it.name }, { it.domain.name } )

        def connectionDbSession = sessionFactory_connections.getCurrentSession()
        List<ServiceContract> takContracts = connectionDbSession.createCriteria(ServiceContract.class).list()

        def modelsStructure = createModelStructure(domainDbSession)
        def groupedViewModels = sort(buildInteractionSummaryViewModels(modelsStructure, interactionMap, takContracts))

        Tak takProd = takObjects.find { it.id == 1 }
        Tak takTest = takObjects.find { it.id == 2 }

        return [takProdName: takProd?.name,
                takTestName: takTest?.name,
                viewModels: groupedViewModels]
    }

    private HashMap<DomainType, List<InteractionSummaryViewModel>> createModelStructure(domainDbSession) {
        List<DomainType> types = domainDbSession.createCriteria(DomainType.class).list()

        def groupedModels = new HashMap<DomainType, List<InteractionSummaryViewModel>>()
        types.each { type -> groupedModels.put(type, new LinkedList<InteractionSummaryViewModel>()) }

        return groupedModels
    }

    private HashMap<DomainType, List<InteractionSummaryViewModel>> buildInteractionSummaryViewModels(
            HashMap<DomainType, List<InteractionSummaryViewModel>> modelStructure, HashMap interactionMap, ArrayList takContracts){
        // interactionMap is a nested HashMap like this: interactionMap[String interactionName][String domainName]
        // The values are an ArrayList<Interaction> with the different versions of the interaction

        def prodContracts = takContracts.findAll { it.tak.id == 1 } . collect { it.namespace }
        def testContracts = takContracts.findAll { it.tak.id == 2 } . collect { it.namespace }

        interactionMap.keySet().each {
            String interactionName -> interactionMap[interactionName].keySet().each {
                String domainName ->
                    ArrayList<Interaction> interactionVersions = interactionMap[interactionName][domainName]

                    if (!interactionVersions.any { it.interactionDescriptions.size() > 0 }) {
                        return
                    }

                    def viewModel = new InteractionSummaryViewModel()
                    viewModel.name = StringFormatting.injectSoftLineBreaksBeforeCapitals(interactionName.replace("Interaction", ""))
                    viewModel.domainName = domainName
                    viewModel.domainPage = domainName.replace(':', '_').replace('.', '_') + ".html"

                    ArrayList<String> responderContracts = interactionVersions.collect { it.responderContract.namespace }

                    viewModel.takProd = responderContracts.any { responderContract ->
                        prodContracts.contains(responderContract)
                    }

                    viewModel.takTest = responderContracts.any { responderContract ->
                        testContracts.contains(responderContract)
                    }

                    def allInteractionDescriptions = interactionVersions.collectMany { it.interactionDescriptions }

                    InteractionDescription description = allInteractionDescriptions.sort {
                        it.lastChangedDate
                    }.last()

                    if (description.description) {
                        viewModel.description = StringFormatting.cutStringIfLongerThan(description.description, 120)
                    }
                    viewModel.lastUpdated = description.lastChangedDate

                    def type = interactionVersions.first().domain.domainType
                    modelStructure.get(type)?.add(viewModel);

            }
        }

        return modelStructure
    }

    private Map sort(HashMap<DomainType, List<InteractionSummaryViewModel>> modelStructure) {
        modelStructure.each { group ->
            group.value.sort(
                    { model -> model.name })
        }

        modelStructure.sort({ it.key.id })
    }
}

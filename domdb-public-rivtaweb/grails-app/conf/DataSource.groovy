dataSource_domains {
    readOnly = true
    driverClassName = "com.mysql.jdbc.Driver"
    // url, username and password is set in ~/.grails/domdb-config.groovy
    // (see "grails.config.locations" in Config.groovy)
}

dataSource_connections {
    readOnly = true
    driverClassName = "com.mysql.jdbc.Driver"
    // url, username and password is set in ~/.grails/domdb-config.groovy
    // (see "grails.config.locations" in Config.groovy)
}

hibernate {
    //cache.use_second_level_cache = true
    //cache.use_query_cache = false
    //cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
    singleSession = true // configure OSIV singleSession mode
    flush.mode = 'manual' // OSIV session flush mode outside of transactional context
    naming_strategy = 'se.rivta.domdb.domain.hibernate.DbNamingStrategy'
    dialect = 'org.hibernate.dialect.MySQLDialect'
}

environments {
    production {
        dataSource_domains {
            properties {
                // See http://grails.org/doc/latest/guide/conf.html#dataSource for documentation
                jmxEnabled = true
                initialSize = 1
                maxActive = 10
                minIdle = 1
                maxIdle = 5
                maxWait = 10000
                maxAge = 10 * 60000
                timeBetweenEvictionRunsMillis = 5000
                minEvictableIdleTimeMillis = 60000
                validationQuery = "SELECT 1"
                validationQueryTimeout = 3
                validationInterval = 15000
                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = false
                jdbcInterceptors = "ConnectionState"
                defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
            }
        }
        dataSource_connections {
            properties {
                // See http://grails.org/doc/latest/guide/conf.html#dataSource for documentation
                jmxEnabled = true
                initialSize = 1
                maxActive = 10
                minIdle = 1
                maxIdle = 5
                maxWait = 10000
                maxAge = 10 * 60000
                timeBetweenEvictionRunsMillis = 5000
                minEvictableIdleTimeMillis = 60000
                validationQuery = "SELECT 1"
                validationQueryTimeout = 3
                validationInterval = 15000
                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = false
                jdbcInterceptors = "ConnectionState"
                defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
            }
        }
    }
}
class UrlMappings {

	static mappings = {
        "/domains/index.html"(controller: "Domain", action: "index")
        "/domains/${domainName}.html"(controller: "Domain", action: "show")
        "/domains/type-${domainType}.html"(controller: "Domain", action: "showDomainTypeInfo")

        "/domains/interaction_index.html"(controller: "Interaction", action: "index")
        "/"(redirect: "/domains")
        "/domains"(redirect: "/domains/index.html")
        //"500"(view:'/error')

    }
}

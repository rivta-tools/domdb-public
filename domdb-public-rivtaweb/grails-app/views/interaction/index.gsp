<html>
<head>
    <meta name="layout" content="main" />
	<title>Index över tjänstekontrakt</title>
</head>
<body>
<div class="domainWrapper">
	<h1>Tjänstekontrakt</h1>
	<div class="ingress">Här hittar du en förteckning över samtliga tjänstekontakt. I tabellen kan du också se om tjänstekontrakten är installerade i den nationella Tjänsteplattformen eller inte.</div>
	<p> </p>
	Informationen på denna sida är direkt hämtad från WSDL-filer i subversion samt tjänsteadresseringskatalogerna i den nationella Tjänsteplattformen. Klicka på länkarna i tabellen för mer information.
	<br>
	<a href="index.html">Se även förteckning över alla tjänstedomäner.</a>
	<p> </p>

    <g:each in="${viewModels}" var="modelGroup">

        <div class="ingress">
            <p><a href="type-${modelGroup.key.id}.html">${modelGroup.key.pluralName}</a></p>
        </div>

        <table class="interactions">
        <thead>
        <tr>
            <th>Tjänstekontrakt</th>
            <th>Beskrivning</th>
            <th>Engelskt domännamn</th>
            <th>${takProdName}</th>
            <th>${takTestName}</th>
        </tr>
        </thead>
        <g:each in="${modelGroup.value}" status="i" var="viewModel">
            <tr>
                <td>${raw(viewModel.name)}</td>
                <td>${fieldValue(bean: viewModel, field: "description")}</td>
                <td><a href="${viewModel.domainPage}">${fieldValue(bean: viewModel, field: "domainName")}</a></td>
                <td class="env"><g:if test="${viewModel.takProd}">X</g:if></td>
                <td class="env"><g:if test="${viewModel.takTest}">X</g:if></td>
            </tr>
        </g:each>
        </table>
    </g:each>
    </div>
</body>
</html>

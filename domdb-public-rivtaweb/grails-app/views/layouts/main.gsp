<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="sv" lang="sv">
<head>
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin-ext" rel="../css/Normal.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link href="../css/Normal.css" rel="stylesheet" type="text/css" />
    <link href="../css/domains.css" rel="stylesheet" type="text/css" />
    <title><g:layoutTitle default="Grails"/></title>
    <g:layoutHead/>
</head>
<body>
<g:layoutBody/>
<p><b>Senast uppdaterad </b><g:formatDate format="yyyy-MM-dd" date="${new Date()}"/></p>
</body>
</html>
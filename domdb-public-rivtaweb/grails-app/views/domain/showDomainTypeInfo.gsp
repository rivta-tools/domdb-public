<%@ page import="se.rivta.domdb.domain.core.DomainType" %>
<html>
<head>
    <meta name="layout" content="main" />
    <title>${domainType.name}</title>
</head>
<body>
<div class="domainWrapper">
    <h1>${domainType.name}</h1>
    <div class="ingress">
        ${raw(domainType.description)}
    </div>
</div>
</body>
</html>
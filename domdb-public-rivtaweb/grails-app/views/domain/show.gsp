<%@ page import="se.rivta.domdb.publ.rivtaweb.DomainDetailsViewModel" %>
<html>
<head>
	<meta name="layout" content="main" />
	<title><g:fieldValue bean="${viewModel}" field="englishName"/></title>
	<!-- target="_top" because IE wants to open Word docs embedded in the browser
			and refuses when we are inside an iframe on inera.se -->
	<base target="_top" />
</head>
<body>
<div class="domainWrapper">
<h1>
	<g:if test="${viewModel.swedishShortName}">
		<g:fieldValue bean="${viewModel}" field="swedishShortName"/> -
	</g:if>
	<g:fieldValue bean="${viewModel}" field="englishName"/>
</h1>

<g:if test="${viewModel.description}">
	<h2>Beskrivning</h2>
	<p>${raw(viewModel.description)}</p>
</g:if>

	<h2>Domäntyp</h2>
	<p><a href="type-${viewModel.domainType.aValue}.html">${viewModel.domainType.bValue}</a></p>

	<h2>Länkar</h2>
	<ul id="externalLinks">
		<g:if test="${viewModel.infoPageUrl}">
			<li><a href="${viewModel.infoPageUrl}">Informationssida</a></li>
		</g:if>
		<g:if test="${viewModel.issueTrackerUrl}">
			<li><a href="${viewModel.issueTrackerUrl}">Ärendehantering</a></li>
		</g:if>
		<g:if test="${viewModel.sourceCodeUrl}">
			<li><a href="${viewModel.sourceCodeUrl}">Källkod</a></li>
		</g:if>
	</ul>

<h2>Tjänstekontrakt</h2>
<p>Tabellen visar vilka versioner av tjänstekontrakt som förekommer i de olika versionerna av tjänstedomänen.</p>
    <g:render template="contracts" bean="${viewModel.serviceContracts}" var="viewModel"/>

<h2>Specifikationer</h2>
<g:render template="specifications" bean="${viewModel.specifications}" var="viewModel"/>

<h2>Granskningar</h2>
<g:if test="${viewModel.reviews && viewModel.reviews.rows.size() > 0}">
	<g:render template="reviews" bean="${viewModel.reviews}" var="viewModel"/>
</g:if>
<g:else>
    <em>Inga granskningar har gjorts av denna tjänstedomän.</em>
</g:else>

<h2>Anslutningar</h2>
<p>Följande tabeller visar vilka tjänstekomponenter som är anslutna mot respektive tjänstekontrakt i den nationella tjänsteplattformen.</p>
<h3>Tjänsteproducenter</h3>
<g:if test="${viewModel.producers && viewModel.producers.rows.size() > 0}">
    <g:render template="connections" bean="${viewModel.producers}" var="viewModel"/>
</g:if>
<g:else>
	<em>Inga tjänsteproducenter finns anslutna i den nationella tjänsteplattformens produktionsmiljö.</em>
</g:else>

<h3>Tjänstekonsumenter</h3>
<g:if test="${viewModel.consumers && viewModel.consumers.rows.size() > 0}">
	<g:render template="connections" bean="${viewModel.consumers}" var="viewModel"/>
</g:if>
<g:else>
	<em>Inga tjänstekonsumenter finns anslutna i den nationella tjänsteplattformens produktionsmiljö.</em>
</g:else>

</div>
</body>
</html>

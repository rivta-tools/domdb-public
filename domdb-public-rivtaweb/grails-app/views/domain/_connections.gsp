<%@ page import="se.rivta.domdb.publ.rivtaweb.TableViewModel" %>

<table class="connections">
    <thead>
    <tr>
        <th></th>
        <g:each in="${viewModel.headings.values()}" var="heading" >
            <th class="contract"><span>${raw(heading)}</span></th>
        </g:each>
    </tr>
    </thead>
    <g:each in="${viewModel.rows.values().sort {it.title}}" var="row" status="index" >
        <tr>
            <td class="firstColumn">
                <strong><g:fieldValue field="title" bean="${row}"/></strong><br/>
                <g:fieldValue field="description" bean="${row}"/>
            </td>
            <g:each in="${viewModel.headings.keySet()}" var="heading" >
                <td>${row.cells[heading]?.join(", ")}</td>
            </g:each>
        </tr>
    </g:each>
</table>
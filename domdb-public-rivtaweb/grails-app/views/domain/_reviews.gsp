<%@ page import="se.rivta.domdb.publ.rivtaweb.TableViewModel" %>

<g:set var="sortedKeys" value="${viewModel.headings.keySet().sort()}"/>
<table class="reviews">
    <thead>
    <tr>
        <th></th>
        <th colspan="${viewModel.headings.size()}">Domänversion</th>
    </tr>
    <tr>
        <th></th>
        <g:each in="${sortedKeys}" var="key" >
            <th class="version">${viewModel.headings[key]}</th>
        </g:each>
    </tr>
    </thead>
    <g:each in="${viewModel.rows.values().sort {it.title}}" var="row" >
        <tr>
            <td class="firstColumn">
                <strong><g:fieldValue field="title" bean="${row}"/></strong>
            </td>
            <g:each in="${sortedKeys}" var="key" >
                <g:set var="cell" value="${row.cells[key]}" />
                <td>
                <g:if test="${cell}">
                    <g:if test="${cell.url}">
                        <a href="${cell.url}">${cell.name}</a>
                    </g:if>
                    <g:else>
                        <g:fieldValue field="name" bean="${cell}"/>
                    </g:else>
                </g:if>
                </td>
            </g:each>
        </tr>
    </g:each>
</table>
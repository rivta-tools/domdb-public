<%@ page import="se.rivta.domdb.publ.rivtaweb.TableViewModel" %>

<table class="specifications">
    <g:set var="sortedKeys" value="${viewModel.headings.keySet().sort()}"/>
    <thead>
    <tr>
        <th></th>
        <th colspan="${viewModel.headings.size()}">Domänversion</th>
    </tr>
    <tr>
        <th></th>
        <g:each in="${sortedKeys}" var="key" >
            <th class="version">${viewModel.headings[key]}</th>
        </g:each>
    </tr>
    </thead>
    <tr>
        <g:set var="row" value="${viewModel.rows["TKB"]}"/>
        <td class="firstColumn"><strong><g:message code="domain.specifications.TKB" default="TKB" /></strong></td>
        <g:each in="${sortedKeys}" var="key" >
            <g:set var="file" value="${row.cells[key]}"/>
            <td>
                <g:if test="${file}">
                    <a href="${file.url}"><img src="../images/icon_doc.png"/> </a>
                </g:if>
            </td>
        </g:each>
    </tr>
    <tr>
        <g:set var="row" value="${viewModel.rows["AB"]}"/>
        <td class="firstColumn"><strong><g:message code="domain.specifications.AB" default="AB" /></strong></td>
        <g:each in="${sortedKeys}" var="key" >
            <g:set var="file" value="${row.cells[key]}"/>
            <td>
                <g:if test="${file}">
                    <a href="${file.url}"><img src="../images/icon_doc.png"/> </a>
                </g:if>
            </td>
        </g:each>
    </tr>
    <tr>
        <g:set var="row" value="${viewModel.rows["IS"]}"/>
        <td class="firstColumn"><strong><g:message code="domain.specifications.IS" default="IS" /></strong></td>
        <g:each in="${sortedKeys}" var="key" >
            <g:set var="file" value="${row.cells[key]}"/>
            <td>
                <g:if test="${file}">
                    <a href="${file.url}"><img src="../images/icon_doc.png"/> </a>
                </g:if>
            </td>
        </g:each>
    </tr>
    <tr>
        <g:set var="row" value="${viewModel.rows["ZIP"]}"/>
        <td class="firstColumn"><strong><g:message code="domain.specifications.ZIP" default="ZIP" /></strong></td>
        <g:each in="${sortedKeys}" var="key" >
            <g:set var="file" value="${row.cells[key]}"/>
            <td>
                <g:if test="${file}">
                    <a href="${file.url}"><img src="../images/icon_zip.png"/> </a>
                </g:if>
            </td>
        </g:each>
    </tr>

</table>
<html>
<head>
    <meta name="layout" content="main" />
	<title>Index över tjänstedomäner</title>
</head>
<body>
<div class="domainWrapper">
	<h1>Tjänstedomäner</h1>
	<div class="ingress">
        <p>Här hittar du en förteckning över samtliga tjänstedomäner. I tabellen kan du också se om de är installerade i den nationella Tjänsteplattformen eller inte.</p></div>
	<p>
	Informationen på denna sida är hämtad från subversion, tjänstekontraktsbeskrivningar samt tjänsteadresseringskatalogerna i den nationella tjänsteplattformen. Klicka på länkarna i tabellen för mer information.
    </p>
	<br>
	<a href="interaction_index.html">Se även förteckning över samtliga tjänstekontrakt.</a>
	<p> </p>
    <g:each in="${viewModels}" var="modelGroup">
        <div class="ingress">
            <p><a href="type-${modelGroup.key.id}.html">${modelGroup.key.pluralName}</a></p>
        </div>
        <table class="domains">
            <thead>
            <tr>
                <th>Svenskt kortnamn</th>
                <th>Svenskt domännamn</th>
                <th>Engelskt domännamn</th>
                <th>${takProdName}</th>
                <th>${takTestName}</th>
            </tr>
            </thead>
            <g:each in="${modelGroup.value}" status="i" var="viewModel">
                <tr>

                    <td>${fieldValue(bean: viewModel, field: "swedishShortName")}</td>
                    <td>${fieldValue(bean: viewModel, field: "swedishLongName")}</td>
                    <td><a href="${viewModel.detailsPage}">${fieldValue(bean: viewModel, field: "englishName")}</a></td>
                    <td class="env"><g:if test="${viewModel.takProd}">X</g:if></td>
                    <td class="env"><g:if test="${viewModel.takTest}">X</g:if></td>
                </tr>
            </g:each>
        </table>
    </g:each>
</div>
</body>
</html>

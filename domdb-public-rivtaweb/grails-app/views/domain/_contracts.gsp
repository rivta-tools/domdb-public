<%@ page import="se.rivta.domdb.publ.rivtaweb.TableViewModel" %>

<g:set var="sortedKeys" value="${viewModel.headings.keySet().sort()}"/>

<table class="contracts">
    <thead>
    <tr>
        <th></th>
        <th colspan="${viewModel.headings.size()}">Domänversion</th>
    </tr>
    <tr>
        <th></th>
        <g:each in="${sortedKeys}" var="key" >
            <th class="version">${viewModel.headings[key]}</th>
        </g:each>
    </tr>
    </thead>
    <g:each in="${viewModel.rows.values().sort {it.title}}" var="row" >
        <tr>
            <td class="firstColumn">
                <strong><g:fieldValue field="title" bean="${row}"/></strong><br/>

                <g:if test="${row.description}">
                    <g:fieldValue field="description" bean="${row}"/>
                </g:if>
                <g:else>
                    <em>Ingen beskrivning hittades.</em>
                </g:else>
            </td>
            <g:each in="${sortedKeys}" var="key" >
                <td>${row.cells[key]}</td>
            </g:each>
        </tr>
    </g:each>
</table>